## Test environments and R CMD check results

### Our developers checked on their local systems:

- local R installation, R 4.0.2, macOS 10.14.6
  `devtools::check(cran = TRUE)`
  
    `0 errors ✓ | 0 warnings ✓ | 0 notes ✓`
    
- local R installation, R 4.0.3, Windows 10 x64

    `0 errors ✓ | 0 warnings ✓ | 0 notes ✓`

- local R installation, R 3.5.2, Debian GNU/Linux 10 (buster)

  `0 errors ✓ | 0 warnings ✓ | 0 notes ✓`

### win-builder, CI/CD services:

- Ubuntu on travis-ci, R 4.0.2 --> Ok.
- gitlab our own ci/cd image  --> Ok.
  - debian:bullseye and R 4.0.4 (x86_64-pc-linux-gnu (64-bit))
- win-builder (devel, stable, oldstable) --> (Ok, Ok, Ok)

## Additional checks on r-hub:

We have set some special options for r-hub:

- `_R_CHECK_FORCE_SUGGESTS_` was set to `false`, because, 
the `vdiffr`-dependency still causes r-hub problems. 

- `check_args = ""` was set to also check the vignettes to
prevent a regression on vignette problems in absence of 
`pandoc` or `pandoc-cite`.


- `_R_CHECK_TESTS_NLINES_` was needed to trace problems on specific platforms.

### `Solaris`

`rhub::check_on_solaris(check_args = "", env_vars = c("_R_CHECK_FORCE_SUGGESTS_" = "false"))`

```
─  Building package
─  Uploading package
─  Preparing build, see status at
   https://builder.r-hub.io/status/dataquieR_1.0.5.tar.gz-ef9f22162b584e5083c61c0d07bcd6aa
─  Build started
─  Downloading package
─  Setting up home directory
─  Running check
─  Installing package dependencies
─  Running R CMD check
─  using log directory ‘/export/home/XM7nBPY/dataquieR.Rcheck’ (38m 15.1s)
─  using R version 4.0.4 (2021-02-15)
─  using platform: i386-pc-solaris2.10 (32-bit)
─  using session charset: UTF-8
✔  checking for file ‘dataquieR/DESCRIPTION’ (1.4s)
─  this is package ‘dataquieR’ version ‘1.0.5’
─  package encoding: UTF-8
✔  checking package namespace information
N  checking package dependencies (2.7s)
   Package suggested but not available for checking: ‘vdiffr’
✔  checking if this is a source package
✔  checking if there is a namespace
✔  checking for executable files (1.3s)
✔  checking for hidden files and directories
✔  checking for portable file names
✔  checking for sufficient/correct file permissions
✔  checking whether package ‘dataquieR’ can be installed (10.7s)
✔  checking installed package size
✔  checking package directory
✔  checking ‘build’ directory
✔  checking DESCRIPTION meta-information
✔  checking top-level files
✔  checking for left-over files
✔  checking index information (1.2s)
✔  checking package subdirectories
✔  checking R files for non-ASCII characters
✔  checking R files for syntax errors
✔  checking whether the package can be loaded (1.4s)
✔  checking whether the package can be loaded with stated dependencies (1.5s)
✔  checking whether the package can be unloaded cleanly (1.2s)
✔  checking whether the namespace can be loaded with stated dependencies
✔  checking whether the namespace can be unloaded cleanly (1.4s)
✔  checking loading without being on the library search path (1.4s)
✔  checking dependencies in R code (3s)
✔  checking S3 generic/method consistency (2.5s)
✔  checking replacement functions (1.2s)
✔  checking foreign function calls (2.9s)
✔  checking R code for possible problems (12.3s)
✔  checking Rd files (1.3s)
✔  checking Rd metadata
✔  checking Rd cross-references
✔  checking for missing documentation entries (1.2s)
✔  checking for code/documentation mismatches (3.7s)
✔  checking Rd \usage sections (3.8s)
✔  checking Rd contents
✔  checking for unstated dependencies in examples
✔  checking installed files from ‘inst/doc’
✔  checking files in ‘vignettes’
✔  checking examples (9.1s)
✔  checking for unstated dependencies in ‘tests’
─  checking tests
✔  Running ‘testthat.R’ (1m 6.1s)
✔  checking for unstated dependencies in vignettes (1m 6.1s)
✔  checking package vignettes in ‘inst/doc’
─  checking running R code from vignettes
     ‘DQ-report-example.Rmd’ using ‘UTF-8’... OK
    NONE
✔  checking re-building of vignette outputs (9.1s)
✔  checking PDF version of manual (4.1s)
   
─  Done with R CMD check
    
> 
```

The note about `vdiffr` is known. This package is not available on many
r-hub-platforms because of unmet system dependencies. In absence of `vdiffr`,
some tests are skipped.

### `Highsierra`

`rhub::check(platform = "macos-highsierra-release-cran", env_vars = c("_R_CHECK_TESTS_NLINES_" = "0"))`

There seem to be problems on `rhub` for that platform currently:

```
[...]
2136#> installation of package ‘ggsignif’ had non-zero exit status
[...]
```
